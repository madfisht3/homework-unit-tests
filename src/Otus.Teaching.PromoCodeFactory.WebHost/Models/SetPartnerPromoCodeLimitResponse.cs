﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class SetPartnerPromoCodeLimitResponse
    {
        public Guid Id { get; set; }
        public Guid LimitId { get; set; }
    }
}
